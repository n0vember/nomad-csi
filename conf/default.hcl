data_dir = "/home/fred/twp/nomad/data"
plugin_dir = "/home/fred/twp/nomad/plugins"
plugin "docker" {
  config {
    allow_privileged = true
    volumes {
      enabled      = true
    }
  }
}
