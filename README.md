# Nomad and CSI

## start nomad server

You must install nomad. On Archlinux, `pacman -S nomad`. Check nomadproject.io for other distributions.

In a terminal run `./nomad_start`. It starts a nomad cluster in dev mode (1 node being server and client).

## Minio

### testing

You need a runing minio server (We could setup one on the nomad client, but this would make things confusing).

In the minio interface, create an API token. Fill the details in `s3/volume.hcl`.

You can then open an other terminal and go straight through the commands:

```
cd s3
export NOMAD_ADDR=http://localhost:4646
nomad run controler.nomad # create the CSI controller
nomad run node.nomad # create the CSI node
nomad volume create volume.hcl
```

At this point you can see on the minio interface a bucket has been created.

You can then run a batch job that will create an empty file in the bucket:

```
nomad run job.nomad
```

### clean-up

To clean everything up you can do the following:

```
nomad job stop -purge s3-test
nomad volume delete test-vol
nomad job stop plugin-s3-node
nomad job stop plugin-s3-controler
```

## Synology

### testing

For this one, you will need a synology NAS. The synology CSI uses the iscsi capability of the NAS. There is a iscsi CSI, but this one worked just fine.

As this uses iscsi, you will need tools on the client. Package list below is for Archlinux, but you should find equivalent versions for your distribution:
- open-iscsi
- lsscsi
- sg3_utils
- multipath-tools

The iscsid service needs to be running. It can be launched with `sudo systemctl start iscsid`.

You will need credentials for a user with administrator rights (or at least the permissions to manipulate iscsi LUNs). Set up the credentials as well as IP and port for the NAS in `synology/monolith.nomad` (in the template section). If you don't want to use volume1, you can also adapt `synology/volume.hcl`.

Once setup, you can proceed:

```
cd synology
export NOMAD_ADDR=http://localhost:4646
nomad run monolith.nomad # the CSI plugin runs controler and node all in one
nomad volume create volume.hcl
nomad run prepare.nomad
nomad run job.nomad
```

To see the result, you can navigate through the nomad interface in the jobs, selecting the syno-test job, allocations tab and clicking on the allocation id. You will have at the bottom a direct link to nginx service.

### clean-up

To clean everything up you can do the following:

```
nomad job stop -purge syno-test
nomad job stop -purge syno-prepare
nomad volume delete example
nomad job stop -purge synology-csi
```

You can also stop the iscsid service.
