job "syno-test" {
  datacenters = ["dc1"]
  type        = "service"
  group "main" {
    count = 1
    network {
      port "http" {
        to = 80
      }
    }
    volume "example" {
      type            = "csi"
      source          = "example"
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }
    task "nginx" {
      driver = "docker"
      config {
        image = "nginx:latest"
        ports = ["http"]
      }
      volume_mount {
        volume      = "example"
        destination = "/usr/share/nginx/html"
        read_only   = false
      }
      resources {
        cpu    = 256
        memory = 512
      }
    }
  }
}
