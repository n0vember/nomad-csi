job "syno-prepare" {
  datacenters = ["dc1"]
  type        = "batch"
  group "main" {
    count = 1
    volume "example" {
      type            = "csi"
      source          = "example"
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }
    task "prepare" {
      driver = "docker"
      config {
        image = "alpine:latest"
        command = "sh"
        args = ["/run.sh"]
        mount {
          type     = "bind"
          source   = "local/run.sh"
          target   = "/run.sh"
          readonly = true
        }
      }
      template {
        data        = <<EOH
echo synology csi test > /usr/share/nginx/html/index.html
EOH
      destination = "local/run.sh"
      }
      volume_mount {
        volume      = "example"
        destination = "/usr/share/nginx/html"
        read_only   = false
      }
      resources {
        cpu    = 256
        memory = 512
      }
    }
  }
}
