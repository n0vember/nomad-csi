job "synology-csi" {
  datacenters = ["dc1"]
  type        = "system"
  node_pool = "default"

  group "controller" {

    task "plugin" {
      driver = "docker"
      config {
        image        = "synology/synology-csi:v1.1.3"
        privileged   = true
        network_mode = "host"
        mount {
          type     = "bind"
          source   = "/"
          target   = "/host"
          readonly = false
        }
        mount {
          type     = "bind"
          source   = "local/csi.yaml"
          target   = "/etc/csi.yaml"
          readonly = true
        }
        args = [
          "--endpoint",
          "unix://csi/csi.sock",
          "--client-info",
          "/etc/csi.yaml"
        ]
      }
      template {
        data        = <<EOH
---
clients:
  - host: SYNOLOGY_IP
    port: 5000
    https: false
    username: SYNOLOGY_USER
    password: SYNOLOGY_PASSWORD
EOH
      destination = "local/csi.yaml"
      }
      csi_plugin {
        id        = "synology"
        type      = "monolith"
        mount_dir = "/csi"
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}
