job "s3-test" {
  datacenters = ["dc1"]
  type        = "batch"
  group "main" {
    count = 1
    volume "test-vol" {
      type            = "csi"
      source          = "test-vol"
      attachment_mode = "file-system"
      access_mode     = "multi-node-multi-writer"
    }
    task "alpine" {
      driver = "docker"
      config {
        image = "alpine:latest"
        command = "touch"
        args = [
          "/data/file",
        ]

      }
      volume_mount {
        volume      = "test-vol"
        destination = "/data"
        read_only   = false
      }
      resources {
        cpu    = 256
        memory = 512
      }
    }
  }
}
