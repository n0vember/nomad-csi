id = "test-vol"
name = "test-vol"
type = "csi"
plugin_id = "s3"
external_id = "test-vol"

capability {
  access_mode     = "multi-node-multi-writer"
  attachment_mode = "file-system"
}

secrets {
  accessKeyID = "xxxxxxxxxxxxxxxxxxxx"
  secretAccessKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  endpoint        = "http://xxxxxxxxxxxx:9000"
}

parameters {
  mounter = "s3fs"
}
